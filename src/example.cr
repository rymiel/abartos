require "http/server"
require "json"
require "log"
require "./abartos"

pp! Abartos::Application::Interaction::Response.new(Abartos::Application::Interaction::Response::Pong.new).to_json

APP_ENV = File.open("env.json") { |file| JSON.parse(file) }.as_h

def application_from_env(name)
  app_info = APP_ENV["applications"][name]
  Abartos::Application.new(client_id: app_info["client_id"].as_s, client_secret: app_info["client_secret"].as_s, public_key: app_info["pubkey"].as_s)
end

def past_oauth(name)
  Array(Abartos::Application::WebhookAccessTokenResponse).from_json APP_ENV["applications"][name]["oauth"].to_json
end

a = application_from_env "Aristotle"

echo_service = a.add_service "echo" do |interaction, uuid|
  pp! "In the service!"
  pp! interaction.member
  Abartos::Interaction::Response.ephemeral_message "Do you hear the echo, #{interaction.member.user.try &.username}? This was message ID #{uuid}."
end

s = HTTP::Server.new([
  HTTP::ErrorHandler.new,
  HTTP::LogHandler.new
]) { |ctx|
  if ctx.request.path == "/digest" && ctx.request.method == "POST"
    a.digest_interaction ctx
  elsif ctx.request.path == "/oauth" && ctx.request.method == "GET"
    code = ctx.request.query_params["code"]
    guild = ctx.request.query_params["guild_id"]
    used_path = "https://" + ctx.request.headers["Host"] + ctx.request.path

    access = a.exchange_webhook_access_token code, used_path

    if access.nil?
      ctx.response.respond_with_status HTTP::Status::UNAUTHORIZED, "Invalid oauth token"
    else
      APP_ENV["applications"]["Aristotle"]["oauth"].as_a << JSON.parse access.to_json
      File.open("env.json", "w") { |file| JSON.build file, 2 { |j| APP_ENV.to_json j } }
      ctx.response.respond_with_status HTTP::Status::OK, "Authorized: #{access.webhook.name}"
    end
  elsif ctx.request.path.starts_with?("/execute/") && ctx.request.method == "GET"
    index, message = ctx.request.path.lchop("/execute/").split("/", 2)
    r = past_oauth("Aristotle")[index.to_i]
    wh = Abartos::Application::Webhook.new a, r.webhook
    execute_r = wh.execute(message + " was brought to you by [abartos](https://abrts.hydra.rymiel.space/)", components: [
      Abartos::Component::ActionRow.new(
        a.button("Test button", :primary, service: echo_service)
      )
    ])
  end
}

Log.setup(:debug)

address = s.bind_tcp "0.0.0.0", 8828
puts "Listening on http://#{address}"
s.listen
