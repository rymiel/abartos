require "nacl/sodium"

module Abartos::NaCl
  def self.zeros(n)
    ("\0" * n).to_slice
  end

  def self.verify(key : Bytes, signature : Slice, message : String)
    arr = [] of UInt8
    arr.concat signature.to_a
    arr.concat message.bytes.to_a
    sig_and_msg = arr.to_unsafe.to_slice(arr.size)

    buffer = self.zeros(sig_and_msg.bytesize)
    buffer_len = self.zeros(64).map(&.to_u64)

    return LibSodium.crypto_sign_ed25519_open(buffer, buffer_len, sig_and_msg, sig_and_msg.bytesize, key)
  end
end