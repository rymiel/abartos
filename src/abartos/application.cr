require "json"
require "uuid"
require "./application/client"
require "./application/webhook"
require "./application/interaction"

module Abartos
  class Application
    @public_key : Bytes
    @client_id : Snowflake
    alias Callback = Interaction::Request::MessageComponent -> Interaction::ResponseData
    alias ServiceCallback = Interaction::Request::MessageComponent, UUID -> Interaction::ResponseData
    @callbacks = {} of UUID => Callback
    @services = {} of String => ServiceCallback
    getter client = Client.new

    class Service
      getter name

      def initialize(@name : String)
      end

      def to_s(io : IO)
        io << @name
      end

      def id(s : String) : String
        "##{name}@#{s}"
      end

      def id(uuid : UUID) : String
        "##{name}@#{uuid}"
      end
    end

    def initialize(*, client_id : Snowflake | String, @client_secret : String, public_key : String | Bytes)
      if public_key.is_a? String
        public_key = public_key.hexbytes
      end
      @public_key = public_key

      if client_id.is_a? String
        client_id = Snowflake.new client_id.to_u64
      end
      @client_id = client_id
    end

    def button(label : String? = nil, style : Symbol? = nil, &block : Callback)
      uuid = UUID.random
      add_callback uuid, block
      Webhook::Component::Button.new(style.try { |e| Webhook::Component::Button::Style.parse? e.to_s }, label: label, custom_id: uuid.to_s)
    end

    def button(label : String? = nil, style : Symbol? = nil, *, service : Service)
      uuid = UUID.random
      Webhook::Component::Button.new(style.try { |e| Webhook::Component::Button::Style.parse? e.to_s }, label: label, custom_id: service.id uuid)
    end

    def add_callback(uuid : UUID, proc : Callback)
      @callbacks[uuid] = proc
    end

    def add_service(id : String, &proc : ServiceCallback) : Service
      @services[id] = proc
      Service.new id
    end

    def digest_interaction(ctx : HTTP::Server::Context)
      body_raw = ctx.request.body.not_nil!.gets_to_end
      body_json = JSON.parse body_raw
  
      request_type = body_json["type"].as_i
  
      verify_signature = ctx.request.headers["X-Signature-Ed25519"]
      verify_timestamp = ctx.request.headers["X-Signature-Timestamp"]
      verify_body = verify_timestamp + body_raw
  
      verification_result = NaCl.verify(@public_key, verify_signature.hexbytes.to_slice, verify_body)
  
      if verification_result == -1
        ctx.response.status_code = 401
        ctx.response.content_type = "text/html; charset=utf-8"
        ctx.response << "invalid request signature"
        return
      end
      
      body = Interaction::Request.from_json body_raw
      ctx.response.content_type = "application/json"
      response_container = case body
      when Interaction::Request::Ping then Interaction::Response::Pong.new
      when Interaction::Request::MessageComponent
        ret = nil
        if body.message.application_id == @client_id
          custom = body.data.try &.[:custom_id]
          pp! custom
          if custom
            if custom[0] == '#'
              service_id, uuid_s = custom.lchop("#").split("@", 2)
              service_cb = @services[service_id]?
              if service_cb
                begin
                  uuid = UUID.new uuid_s
                  ret = service_cb.call(body, uuid)
                rescue ex : ArgumentError
                end
              end
            else
              begin
                uuid = UUID.new custom
                cb = @callbacks[uuid]?
                if cb
                  ret = cb.call(body)
                end
              rescue ex : ArgumentError
              end
            end
          end
        end
        pp! ret
        ret
      else
        pp! request_type
        pp! body_json
        nil
      end
      if response_container.nil?
        ctx.response << "{}"
      else
        ctx.response << Interaction::Response.new(response_container).to_json
      end
    end

    record WebhookReponseObject,
      type : Int32,
      id : Snowflake,
      name : String,
      avatar : String,
      channel_id : Snowflake,
      guild_id : Snowflake,
      application_id : Snowflake,
      token : String,
      url : String do
        include JSON::Serializable
      end
    
    record WebhookAccessTokenResponse,
      access_token : String,
      expires_in : Int32,
      refresh_token : String,
      scope : String,
      token_type : String,
      webhook : WebhookReponseObject do
        include JSON::Serializable
      end

    def exchange_webhook_access_token(code : String, redirect_uri : String) : WebhookAccessTokenResponse?
      payload = {
        "client_id" => @client_id.to_s,
        "client_secret" => @client_secret,
        "grant_type" => "authorization_code",
        "code" => code,
        "redirect_uri" => redirect_uri
      }
      res = @client.post "/oauth2/token", form: payload
      if res.status.ok?
        WebhookAccessTokenResponse.from_json res.body
      else
        pp! res.status
        pp! res.body
        nil
      end
    end
  end
end
