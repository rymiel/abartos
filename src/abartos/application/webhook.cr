class Abartos::Application
  class Webhook
    abstract class Component
      include JSON::Serializable

      alias ActionRowChildren = Button
      alias Type = ActionRow | ActionRowChildren

      property type : Int32
      use_json_discriminator "type", {
        1 => ActionRow,
        2 => Button
      }

      class ActionRow < Component
        property components : Array(Component::ActionRowChildren)
        @type = 1

        def initialize(*, @components)
        end

        def initialize(*children)
          initialize components: children.to_a.map(&.as Component::ActionRowChildren)
        end
      end

      class Button < Component
        @type = 2
        enum Style
          Primary = 1
          Secondary = 2
          Success = 3
          Danger = 4
          Link = 5
        end
        
        @[JSON::Field(converter: Enum::NumericalConverter32(Abartos::Application::Webhook::Component::Button::Style))]
        property style : Style?
        property label : String?
        property custom_id : String?
        property url : String?

        def initialize(@style : Style? = nil, *, @label : String? = nil, @custom_id : String? = nil, @url : String? = nil)
        end

        def self.link(label : String, url : String) : Button
          new(Style::Link, label: label, url: url)
        end
      end
    end
    
    getter id, token

    def self.new(application : Application, reponse_webhook wh : WebhookReponseObject)
      self.new application, wh.id, wh.token
    end

    def initialize(@application : Application, @id : Snowflake, @token : String)
    end

    def execute(message : String) : HTTP::Client::Response
      @application.client.post("/webhooks/#{@id}/#{@token}?wait=true", json: {
        content: message
      })
    end

    def execute(message : String, *, username : String? = nil, components : Array(Component) = [] of Component) : HTTP::Client::Response
      @application.client.post("/webhooks/#{@id}/#{@token}?wait=true", json: {
        content: message,
        username: username,
        components: components
      })
    end
  end
end
