require "json"

module Abartos
  class Application

    module Interaction
      abstract struct Request
        include JSON::Serializable

        use_json_discriminator "type", {
          1 => Ping,
          3 => MessageComponent
        }

        property type : Int32
        property id : Snowflake
        property application_id : Snowflake
        property guild_id : Snowflake?
        property channel_id : Snowflake?
        property version : Int32

        struct Ping < Request
          @type = 1
        end

        struct MessageComponent < Request
          @type = 3
          property message : Resource::Message
          property data : {custom_id: String?, component_type: Int32?}?
          property member : Resource::Member
          property token : String
        end
      end
      abstract struct ResponseData
        include JSON::Serializable
        @[JSON::Field(ignore: true)]
        property type : Int32
      end
      struct Response
        property data : ResponseData

        def initialize(@data)
        end

        def to_json(json : JSON::Builder)
          json.object do
            json.field "type", @data.type
            if !@data.is_a?(Pong)
              json.field "data" do
                @data.to_json json
              end
            end
          end
        end

        struct Pong < ResponseData
          @type = 1

          def initialize
          end
        end

        struct ChannelMessageWithSource < ResponseData
          @[Flags]
          enum CallbackFlag
            Ephemeral = 64
          end
          @type = 4
          property content : String?
          @[JSON::Field(converter: Enum::NumericalConverter32(Abartos::Application::Interaction::Response::ChannelMessageWithSource::CallbackFlag))]
          property flags : CallbackFlag?

          def initialize(@content, @flags)
          end
        end

        def self.ephemeral_message(message : String)
          ChannelMessageWithSource.new message, ChannelMessageWithSource::CallbackFlag::Ephemeral
        end
      end
    end
  end
end
