class Abartos::Application
  class Client
    STANDARD_HEADERS = HTTP::Headers{"User-Agent" => "Abartos Standalone/#{Abartos::VERSION} Crystal/#{Crystal::VERSION}"}
    @client = HTTP::Client.new Abartos::API

    def post(path : String, headers : HTTP::Headers? = nil, *, form : Hash) : HTTP::Client::Response
      absolute_path = Abartos::API.dup.path += path
      headers = headers ? headers.dup.merge!(STANDARD_HEADERS) : STANDARD_HEADERS
      @client.post(absolute_path, headers, form: form)
    end

    def post(path : String, headers : HTTP::Headers? = nil, *, json) : HTTP::Client::Response
      absolute_path = Abartos::API.dup.path += path
      headers = headers ? headers.dup.merge!(STANDARD_HEADERS) : STANDARD_HEADERS
      headers["Content-Type"] = "application/json"
      @client.post(absolute_path, headers, body: json.to_json)
    end
  end
end