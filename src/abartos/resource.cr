require "json"

abstract struct Enum
  class NumericalConverter64(T)
    def self.from_json(j : JSON::PullParser) : T
      T.new j.read_int
    end
    
    def self.to_json(value : T, b : JSON::Builder)
      b.number value.value
    end
  end

  class NumericalConverter32(T)
    def self.from_json(j : JSON::PullParser) : T
      T.new j.read_int.to_i32
    end
    
    def self.to_json(value : T, b : JSON::Builder)
      b.number value.value
    end
  end
end

module Abartos::Resource
  struct Message
    include JSON::Serializable
    include JSON::Serializable::Unmapped

    property id : Snowflake
    property author : User
    property channel_id : Snowflake
    property guild_id : Snowflake?
    property content : String
    property timestamp : Time
    property edited_timestamp : Time?
    property tts : Bool
    property mention_everyone : Bool
    property pinned : Bool
    property webhook_id : Snowflake?
    property application_id : Snowflake?
    property components : Array(Abartos::Application::Webhook::Component)?
  end

  struct User
    @[Flags]
    enum UserFlags : Int64
      DiscordEmployee
      PartneredServerOwner
      HypeSquadEvents
      BugHunterOne
      HouseBravery
      HouseBrilliance
      HouseBalance
      EarlySupporter
      TeamUser
      BugHunterTwo
      VerifiedBot
      EarlyVerifiedBotDeveloper
      DiscordCertifiedModerator
    end

    include JSON::Serializable
    include JSON::Serializable::Unmapped
    
    property id : Snowflake
    property username : String
    property discriminator : String
    property avatar : String?
    property bot : Bool?
    property system : Bool?
    property mfa_enabled : Bool?
    property banner : String?
    property accent_color : String?
    property locale : String?
    @[JSON::Field(converter: Enum::NumericalConverter64(Abartos::Resource::User::UserFlags))]
    property flags : UserFlags?
    property premium_type : Int32?
    @[JSON::Field(converter: Enum::NumericalConverter64(Abartos::Resource::User::UserFlags))]
    property public_flags : UserFlags?
  end

  struct Member
    include JSON::Serializable
    include JSON::Serializable::Unmapped

    property user : User?
    property nick : String?
    property roles : Array(Snowflake)
    property joined_at : Time
    property premium_since : Time?
    property deaf : Bool
    property mute : Bool
    property pending : Bool?
    property permissions : String?
  end
end