require "json"

module Abartos
  struct Snowflake
    def self.new(pull : JSON::PullParser)
      self.new pull.read_string.to_u64
    end

    def initialize(@val : UInt64)
    end

    def timestamp : Time
      Time.unix_ms (@val >> 22) + 1420070400000
    end

    def worker : UInt8
      ((@val & 0x3E0000) >> 17).to_u8
    end

    def process : UInt8
      ((@val & 0x1F000) >> 12).to_u8
    end

    def sequence : UInt16
      ((@val & 0x1F000) >> 12).to_u16
    end

    def inspect(io : IO)
      io << "❄["
      to_s io
      io << "; " << timestamp.to_rfc3339 fraction_digits: 3
      io << " W"
      worker.to_s io
      io << ".P"
      process.to_s io
      io << ".S"
      sequence.to_s io
      io << "]"
    end

    def to_s(io : IO)
      @val.to_s io
    end

    def to_u64()
      @val
    end

    def to_json(b : JSON::Builder)
      b.scalar @val.to_s
    end
  end
end