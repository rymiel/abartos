require "http/server"
require "json"
require "uri"

require "./abartos/nacl"
require "./abartos/snowflake"
require "./abartos/resource"
require "./abartos/application"

module Abartos
  VERSION = "0.1.0"
  API = URI.parse "https://discord.com/api/v9"

  alias Component = Application::Webhook::Component
  alias Interaction = Application::Interaction
end
